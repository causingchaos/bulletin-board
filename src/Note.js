//main component
import React from 'react';
import FaPencil from 'react-icons/lib/fa/pencil';
import FaTrash from 'react-icons/lib/fa/trash';
import FaFloppyO from 'react-icons/lib/fa/floppy-o';

class Note extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        editing: false
      };
      this.edit = this.edit.bind(this);
      this.remove = this.remove.bind(this);
      this.save = this.save.bind(this);
      this.renderForm = this.renderForm.bind(this);
      this.renderDisplay = this.renderDisplay.bind(this);
    }
    edit() {
      this.setState({
        editing: true
      });
      alert('editing note');
    };
    remove() {
      alert('removing');
    };
    save() {
      alert(this._newText.value);
    }
    renderForm() {
      return (
        <div className="note">
          <form>
            <textarea />
            <button onClick={this.save} id="save"><FaFloppyO></FaFloppyO></button>
          </form>
        </div>
      )
    }

    //this.props.children = note: "string"
  // were passing in {note.note}, basically the "child" element of className note, which is just a single note
  // defined in the board constructor
    renderDisplay() {
        return (
            <div className="note">
                <p>{this.props.children}</p>
                <span>
                  <button onClick={this.edit} id="edit"><FaPencil/></button>
                  <button onClick={this.remove} id="remove"><FaTrash/></button>
                </span>
            </div>
        )
    }
    render() {
      //return this.state.editing ? this.renderForm() : this.renderDisplay();

      if(this.state.editing) {
        return this.renderForm();
      }else{
        return this.renderDisplay();
      }
    }
};

export default Note;